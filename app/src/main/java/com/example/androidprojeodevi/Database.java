package com.example.androidprojeodevi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "bookDBTEST1";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "books";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_PAGE_COUNT = "page_count";
    private static final String COLUMN_GENRE = "genre";
    private static final String COLUMN_AUTHOR = "author";
    private static final String COLUMN_PRICE = "price";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_TITLE + " TEXT,"
                + COLUMN_IMAGE + " TEXT,"
                + COLUMN_DESCRIPTION + " TEXT,"
                + COLUMN_PAGE_COUNT + " INTEGER,"
                + COLUMN_GENRE + " TEXT,"
                + COLUMN_AUTHOR + " TEXT,"
                + COLUMN_PRICE + " REAL" + ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, book.getTitle());
        values.put(COLUMN_IMAGE, book.getImage());
        values.put(COLUMN_DESCRIPTION, book.getDescription());
        values.put(COLUMN_PAGE_COUNT, book.getPageCount());
        values.put(COLUMN_GENRE, book.getGenre());
        values.put(COLUMN_AUTHOR, book.getGenre());
        values.put(COLUMN_PRICE, book.getPrice());
        values.put(COLUMN_AUTHOR, book.getAuthor());
        db.insert(TABLE_NAME, null, values);
        db.close();

        return true;
    }

    public Book getBookById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] sutunlar={COLUMN_ID,COLUMN_TITLE,COLUMN_IMAGE,COLUMN_DESCRIPTION,COLUMN_PAGE_COUNT,COLUMN_GENRE,COLUMN_PRICE,COLUMN_AUTHOR};
        Cursor cr = db.query(TABLE_NAME, sutunlar, COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        Book book = null;
        if (cr.moveToFirst()) {
            book = new Book(Integer.parseInt(cr.getString(0)),cr.getString(1),cr.getString(7),cr.getString(3),Integer.parseInt(cr.getString(4)),cr.getString(5),Double.parseDouble(cr.getString(6)),cr.getString(2));
        }
        cr.close();
        db.close();
        return book;
    }



    public List<Book> veriListele(){
        List<Book> veriler=new ArrayList<Book>();
        SQLiteDatabase db=this.getWritableDatabase();
        String[] sutunlar={COLUMN_ID,COLUMN_TITLE,COLUMN_IMAGE,COLUMN_DESCRIPTION,COLUMN_PAGE_COUNT,COLUMN_GENRE,COLUMN_PRICE,COLUMN_AUTHOR};
        Cursor cr=db.query(TABLE_NAME,sutunlar,null,null,null,null,null);
        while(cr.moveToNext()){
            veriler.add(new Book(Integer.parseInt(cr.getString(0)),cr.getString(1),cr.getString(7),cr.getString(3),Integer.parseInt(cr.getString(4)),cr.getString(5),Double.parseDouble(cr.getString(6)),cr.getString(2)));
            System.out.println(cr.getString(2));
        }
        return veriler;
    }

    public boolean deleteBook(int bookId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?", new String[] { String.valueOf(bookId) });
        db.close();

        return true;
    }
    public boolean updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, book.getTitle());
        values.put(COLUMN_AUTHOR, book.getAuthor());
        values.put(COLUMN_DESCRIPTION, book.getDescription());
        values.put(COLUMN_PAGE_COUNT, book.getPageCount());
        values.put(COLUMN_GENRE, book.getGenre());
        values.put(COLUMN_PRICE, book.getPrice());
        values.put(COLUMN_IMAGE, book.getImage());
        int rowsAffected = db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(book.getId())});
        db.close();
        return rowsAffected > 0;
    }

}
