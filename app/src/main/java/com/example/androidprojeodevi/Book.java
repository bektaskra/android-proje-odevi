package com.example.androidprojeodevi;

public class Book {
    private String title;
    private String author;
    private String description;
    private int pageCount;
    private String genre;
    private double price;
    private String image;

    private int id;

    public Book(int id,String title, String author, String description, int pageCount, String genre, double price, String image) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.pageCount = pageCount;
        this.genre = genre;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return this.image;
    }


    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getGenre() {
        return genre;
    }

    public double getPrice() {
        return price;
    }
}
