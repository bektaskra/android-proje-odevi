package com.example.androidprojeodevi;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Yeni Kitap Ekle");

        Button btnAdd = (Button) findViewById(R.id.buttonUpdateBook);
        Button btnBack = (Button) findViewById(R.id.buttonBackFromUpdateBookForm);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Database db = new Database(BookAddActivity.this);

                EditText txtTitle = (EditText) findViewById(R.id.editTextTitleUpdate);
                EditText txtAuthor = (EditText) findViewById(R.id.editTextAuthorUpdate);
                EditText txtGenre = (EditText) findViewById(R.id.editTextGenreUpdate);
                EditText txtPageCount = (EditText) findViewById(R.id.editTextPageCountUpdate);
                EditText txtPrice = (EditText) findViewById(R.id.editTextPriceUpdate);
                EditText txtDescription = (EditText) findViewById(R.id.editTextDescriptionUpdate);

                if (txtTitle.getText().toString().equals("") || txtAuthor.getText().toString().equals("") || txtGenre.getText().toString().equals("") || txtPageCount.getText().toString().equals("") || txtPrice.getText().toString().equals("") || txtDescription.getText().toString().equals("")){
                    Toast.makeText(BookAddActivity.this, "Lütfen tüm alanları doldurun", Toast.LENGTH_SHORT).show();
                }else{
                    boolean check = db.addBook(new Book(0,
                            txtTitle.getText().toString(),
                            txtAuthor.getText().toString(),
                            txtDescription.getText().toString(),
                            Integer.parseInt(txtPageCount.getText().toString()),
                            txtGenre.getText().toString(),
                            Double.parseDouble(txtPrice.getText().toString()),
                            ""));

                    if (check){
                        Toast.makeText(BookAddActivity.this, "Kitap başarıyla eklendi", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(BookAddActivity.this, "Eklenme sırasında bir hata meydana geldi", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // Geri butonuna tıklama olayını tetikle
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}