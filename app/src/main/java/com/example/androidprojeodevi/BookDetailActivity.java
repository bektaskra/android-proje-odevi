package com.example.androidprojeodevi;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BookDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView titleTextView;
    private TextView authorTextView;
    private TextView priceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Kitap verilerini çekme
        int bookId = getIntent().getIntExtra("bookId", -1);
        Database db = new Database(BookDetailActivity.this);
        Book book = db.getBookById(bookId);

        setTitle(book.getTitle());

        // Butonlar:

        // Geri butonunu
        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Önceki sayfaya gitmek için
                onBackPressed();
            }
        });

        // Kaldırma butonunu
        Button btnDelete = (Button) findViewById(R.id.delete_button);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(BookDetailActivity.this)
                        .setTitle("Kitap Kaldırma")
                        .setMessage("Bu kitabı kaldırmak istediğinize emin misiniz?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Kaldır", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Database db = new Database(BookDetailActivity.this);
                                boolean check = db.deleteBook(book.getId());
                                if (check) {
                                    Toast.makeText(BookDetailActivity.this, book.getTitle()+" başarılı bir şekilde kaldırıldı", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(BookDetailActivity.this,MainActivity.class);
                                    startActivity(i);
                                }
                            }
                        })
                        .setNegativeButton("Vazgeç", null).show();
            }
        });



        // Güncelleme butonunu
        Button updateButton = findViewById(R.id.edit_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent intent = new Intent(BookDetailActivity.this, BookUpdateActivity.class);
                intent.putExtra("bookId", bookId);
                startActivity(intent);
            }
        });

        //Veriler:

        // Kitap resmini gösterme
        imageView = findViewById(R.id.book_image);
        imageView.setImageResource(R.drawable.default_book_image);

        // Kitap başlığını gösterme
        titleTextView = findViewById(R.id.book_title);
        titleTextView.setText(book.getTitle());

        // Kitap yazarını gösterme
        authorTextView = findViewById(R.id.book_author);
        authorTextView.setText(book.getAuthor());

        // Kitap açıklamsını gösterme
        authorTextView = findViewById(R.id.book_description);
        authorTextView.setText(book.getDescription());

        // Kitap fiyatını gösterme
        priceTextView = findViewById(R.id.book_price);
        priceTextView.setText("₺" + String.valueOf(book.getPrice()));
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Kitap verilerini çekme
        int bookId = getIntent().getIntExtra("bookId", -1);
        Database db = new Database(BookDetailActivity.this);
        Book book = db.getBookById(bookId);

        setTitle(book.getTitle());

        //Veriler:

        // Kitap resmini gösterme
        imageView = findViewById(R.id.book_image);
        imageView.setImageResource(R.drawable.default_book_image);

        // Kitap başlığını gösterme
        titleTextView = findViewById(R.id.book_title);
        titleTextView.setText(book.getTitle());

        // Kitap yazarını gösterme
        authorTextView = findViewById(R.id.book_author);
        authorTextView.setText(book.getAuthor());

        // Kitap açıklamsını gösterme
        authorTextView = findViewById(R.id.book_description);
        authorTextView.setText(book.getDescription());

        // Kitap fiyatını gösterme
        priceTextView = findViewById(R.id.book_price);
        priceTextView.setText("₺" + String.valueOf(book.getPrice()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // Geri butonuna tıklama olayını tetikle
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}