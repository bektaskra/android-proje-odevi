package com.example.androidprojeodevi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.MenuItem;

public class BookUpdateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_update);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Kitap verilerini çekme
        int bookId = getIntent().getIntExtra("bookId", -1);
        Database db = new Database(BookUpdateActivity.this);
        Book book = db.getBookById(bookId);

        setTitle("Düzenle: "+book.getTitle());

        // Verileri yazdırma

        // Başlık
        EditText txtTitle = (EditText) findViewById(R.id.editTextTitleUpdate);
        txtTitle.setText(book.getTitle());

        // Yazar:
        EditText txtAuthor = (EditText) findViewById(R.id.editTextAuthorUpdate);
        txtAuthor.setText(book.getAuthor());

        // Tür:
        EditText txtGenre = (EditText) findViewById(R.id.editTextGenreUpdate);
        txtGenre.setText(book.getGenre());

        // Sayfa Sayısı:
        EditText txtPageCount = (EditText) findViewById(R.id.editTextPageCountUpdate);
        txtPageCount.setText(String.valueOf(book.getPageCount()));

        // Fiyat:
        EditText txtPrice = (EditText) findViewById(R.id.editTextPriceUpdate);
        txtPrice.setText(String.valueOf(book.getPrice()));

        // Açıklama:
        EditText txtDescription = (EditText) findViewById(R.id.editTextDescriptionUpdate);
        txtDescription.setText(book.getDescription());

        // Butonlar:

        // Kaydet:
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateBook);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Güncellenecek kitap nesnesi
                Book updatedBook = new Book(
                        book.getId(),
                        txtTitle.getText().toString(),
                        txtAuthor.getText().toString(),
                        txtDescription.getText().toString(),
                        Integer.parseInt(txtPageCount.getText().toString()),
                        txtGenre.getText().toString(),
                        Double.parseDouble(txtPrice.getText().toString()),
                        ""
                );

                // Database'i güncelle
                Database db = new Database(BookUpdateActivity.this);
                boolean check = db.updateBook(updatedBook);

                // Başarılı mesajı göster
                if (check) {
                    Toast.makeText(BookUpdateActivity.this, "Başarılı bir şekilde güncellendi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Geri
        Button backButton = findViewById(R.id.buttonBackFromUpdateBookForm);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // Geri butonuna tıklama olayını tetikle
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}