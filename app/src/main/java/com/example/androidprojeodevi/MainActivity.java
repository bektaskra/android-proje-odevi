package com.example.androidprojeodevi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BookAdapter.OnItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Liste:

        ListView listView = (ListView) findViewById(R.id.listView);

        BookAdapter adapter = new BookAdapter(MainActivity.this, new ArrayList<Book>());
        Database db = new Database(MainActivity.this);

        //db.addBook(new Book(0, "Deneme Eserleri", "Bektaş Kara", "Bu kitap örnek olması amacıyla eklenmiştir. Açıklamsı ile bu yüzden uzun tutulmuştur.", 120, "Hikaye", 45.0, "kitaps"));

        List<Book> rows = db.veriListele();

        adapter.addAll(rows);
        listView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

        // Yeni Kitap Ekleme Buton:

        Button btnAddBook = (Button) findViewById(R.id.buttonGoToAddBook);

        btnAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookAddActivity.class);
                startActivity(intent);
            }
        });
    }

    // Geri dönüşlerde listenin yenilenmesi için.
    @Override
    protected void onResume() {
        super.onResume();
        BookAdapter adapter = new BookAdapter(MainActivity.this, new ArrayList<Book>());
        Database db = new Database(MainActivity.this);
        List<Book> rows = db.veriListele();
        adapter.addAll(rows);
        adapter.setOnItemClickListener(this);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }

    // Listede bir kitabın seçilmesini dinlemek için
    @Override
    public void onItemClick(Book book) {
        int bookId = book.getId();
        Intent intent = new Intent(MainActivity.this, BookDetailActivity.class);
        intent.putExtra("bookId", bookId);
        startActivity(intent);
    }
}