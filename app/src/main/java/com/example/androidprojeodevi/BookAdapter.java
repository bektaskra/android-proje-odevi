package com.example.androidprojeodevi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class BookAdapter extends ArrayAdapter<Book> {

    private Context mContext;
    private OnItemClickListener onItemClickListener;

    public BookAdapter(Context context, List<Book> books) {
        super(context, R.layout.book_list_item, books);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.book_list_item, parent, false);
        }

        // Kitap verisini alın
        final Book book = getItem(position);

        // Kitap başlığını göstermek için TextView'i ayarlayın
        TextView titleTextView = convertView.findViewById(R.id.book_title);
        titleTextView.setText(book.getTitle());

        // Kitap yazarını göstermek için TextView'i ayarlayın
        TextView authorTextView = convertView.findViewById(R.id.book_author);
        authorTextView.setText(book.getAuthor());

        // Kitap resmini göstermek için ImageView'i ayarlayın
        ImageView imageView = convertView.findViewById(R.id.book_image);
        imageView.setImageResource(R.drawable.default_book_image);

        // Kitap fiyatını göstermek için TextView'i ayarlayın
        TextView priceTextView = convertView.findViewById(R.id.book_price);
        priceTextView.setText("₺" + String.valueOf(book.getPrice()));

        // Gizli ID değerini göstermek için TextView'i ayarlayın
        TextView idTextView = convertView.findViewById(R.id.book_id);
        idTextView.setText(String.valueOf(book.getId()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(book);
                }
            }
        });

        return convertView;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Book book);
    }
}

